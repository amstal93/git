# Introduction

This project creates a docker image with git (including git-lfs) installed on
[Debian stable](https://wiki.debian.org/DebianStable) (Debian 9/Debian Stretch) with backports.

The image can be used as a base image for other images.

This repository is mirrored to https://gitlab.com/sw4j-net/git
